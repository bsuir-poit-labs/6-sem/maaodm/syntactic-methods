﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace miapr_7
{
    public partial class MainForm : Form
    {
        private readonly bool[] _statuses = new bool[7];
        private static readonly Color OnColor = Color.Lime;
        private static readonly Color OffColor = Color.Green;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnRecognize_Click(object sender, EventArgs e)
        {
            var number = Model.RecognizeNumber(_statuses);
            MessageBox.Show(number != null ? $@"Вы ввели число {number}." : @"Не удалось распознать число.");
        }

        private void panel_Click(object sender, EventArgs e)
        {
            var panel = (Panel) sender;
            var index = Convert.ToInt32(panel.Tag);

            panel.BackColor = _statuses[index] ? OffColor : OnColor;
            _statuses[index] = !_statuses[index];
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            panel0.BackColor = panel1.BackColor = panel2.BackColor =
                panel3.BackColor = panel4.BackColor = panel5.BackColor = panel6.BackColor = OffColor;

            for (var i = 0; i < _statuses.Length; i++)
            {
                _statuses[i] = false;
            }
        }
    }
}