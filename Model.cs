﻿namespace miapr_7
{
    public static class Model
    {
        private static readonly bool[][] NumberStatuses = new[]
        {
            new[] {true, true, true, false, true, true, true},      // 0
            new[] {false, false, true, false, false, false, true},  // 1
            new[] {false, true, true, true, true, true, false},     // 2      
            new[] {false, true, true, true, false, true, true},     // 3      
            new[] {true, false, true, true, false, false, true},    // 4      
            new[] {true, true, false, true, false, true, true},     // 5      
            new[] {true, true, false, true, true, true, true},      // 6      
            new[] {false, true, true, false, false, false, true},   // 7      
            new[] {true, true, true, true, true, true, true},       // 8      
            new[] {true, true, true, true, false, true, true},      // 9      
        };

        public static int? RecognizeNumber(bool[] statuses)
        {
            for (var i = 0; i < NumberStatuses.Length; i++)
            {
                var flag = true;
                for (var j = 0; j < NumberStatuses[i].Length; j++)
                {
                    if (statuses[j] != NumberStatuses[i][j])
                    {
                        flag = false;
                    }
                }

                if (flag)
                {
                    return i;
                }
            }

            return null;
        }
    }
}